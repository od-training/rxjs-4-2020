import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap, switchMap, shareReplay, startWith } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

const BASE_URL = 'http://api.angularbootcamp.com';

export interface Employee {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  hours_worked: number;
  hourly_wage: number;
}

@Injectable({
  providedIn: 'root',
})
export class EmployeesService {
  // could be done with a behavior subject too!
  private selectedEmployeeId = new Subject<number>();

  public selectedEmployee: Observable<Employee>;

  constructor(private httpClient: HttpClient) {
    this.selectedEmployee = this.selectedEmployeeId.pipe(
      switchMap((id) => this.getEmployeeDetail(id)),
      startWith(null),
      shareReplay(1),
      tap((data) => console.log(data))
    );
  }

  loadEmployees() {
    return this.httpClient
      .get(BASE_URL + '/employees')
      .pipe(map((employees: Employee[]) => employees.slice(0, 10)));
  }

  getEmployeeDetail(id: number) {
    return this.httpClient
      .get(BASE_URL + '/employees/' + id)
      .pipe(tap((employee: Employee) => console.log(employee)));
  }

  selectEmployee(employeeId: number) {
    this.selectedEmployeeId.next(employeeId);
  }
}
