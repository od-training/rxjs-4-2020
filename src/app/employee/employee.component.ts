import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee, EmployeesService } from '../employees.service';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
})
export class EmployeeComponent {
  employeeList: Observable<Employee[]>;
  selectedEmployee: Observable<Employee>;

  constructor(public employeesService: EmployeesService) {
    this.employeeList = this.employeesService.loadEmployees();

    this.selectedEmployee = this.employeesService.selectedEmployee;
  }
}
