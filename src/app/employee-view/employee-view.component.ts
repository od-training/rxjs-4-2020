import { Component, Input } from '@angular/core';
import { Employee } from '../employees.service';

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view.component.html',
  styleUrls: ['./employee-view.component.scss'],
})
export class EmployeeViewComponent {
  @Input() employeeDetail: Employee;
}
