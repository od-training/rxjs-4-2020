import { Component, Output, EventEmitter, Input } from '@angular/core';
import { Employee } from '../employees.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss'],
})
export class EmployeeListComponent {
  @Input() employees: Employee[] = [];
  @Output() employeeSelected = new EventEmitter<number>();
}
